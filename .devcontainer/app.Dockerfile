from golang:alpine

WORKDIR /src/api

COPY ./ /src/api

ENV TZ=Europe/Lisbon

RUN apk add --no-cache tzdata