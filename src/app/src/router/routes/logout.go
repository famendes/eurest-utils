package routes

import (
	"eurest-utils-app/src/controllers"
	"net/http"
)

var logoutRoute = Route{
	URI:         "/logout",
	Method:      http.MethodGet,
	Function:    controllers.Logout,
	RequireAuth: true,
}
