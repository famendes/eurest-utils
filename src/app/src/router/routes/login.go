package routes

import (
	"eurest-utils-app/src/controllers"
	"net/http"
)

var loginRoutes = []Route{
	{
		URI:         "/",
		Method:      http.MethodGet,
		Function:    controllers.LoadLogin,
		RequireAuth: false,
	},
	{
		URI:         "/login",
		Method:      http.MethodGet,
		Function:    controllers.LoadLogin,
		RequireAuth: false,
	},
	{
		URI:         "/login",
		Method:      http.MethodPost,
		Function:    controllers.Login,
		RequireAuth: false,
	},
}
