package routes

import (
	"eurest-utils-app/src/controllers"
	"net/http"
)

var homeRoute = Route{
	URI:         "/home",
	Method:      http.MethodGet,
	Function:    controllers.LoadHome,
	RequireAuth: true,
}
