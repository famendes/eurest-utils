package routes

import (
	"eurest-utils-app/src/controllers"
	"net/http"
)

var tpaRoutes = []Route{
	{
		URI:         "/tpa",
		Method:      http.MethodGet,
		Function:    controllers.LoadTPA,
		RequireAuth: true,
	},
	{
		URI:         "/tpa",
		Method:      http.MethodPost,
		Function:    controllers.AddTPA,
		RequireAuth: true,
	},
	{
		URI:         "/tpa/{storeID}",
		Method:      http.MethodGet,
		Function:    controllers.GetTPAsByStore,
		RequireAuth: true,
	},
}
