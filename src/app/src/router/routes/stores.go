package routes

import (
	"eurest-utils-app/src/controllers"
	"net/http"
)

var storesRoutes = []Route{
	{
		URI:         "/stores",
		Method:      http.MethodGet,
		Function:    controllers.GetStores,
		RequireAuth: true,
	},
}
