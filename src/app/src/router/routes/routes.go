package routes

import (
	"eurest-utils-app/src/middlewares"
	"net/http"
	"path/filepath"

	"github.com/gorilla/mux"
)

type Route struct {
	URI         string
	Method      string
	Function    func(http.ResponseWriter, *http.Request)
	RequireAuth bool
}

func Configure(r *mux.Router) *mux.Router {
	routes := loginRoutes
	routes = append(routes, logoutRoute)
	routes = append(routes, homeRoute)
	routes = append(routes, tpaRoutes...)
	routes = append(routes, storesRoutes...)
	routes = append(routes, salesRoutes...)

	for _, route := range routes {
		if route.RequireAuth {
			r.HandleFunc(route.URI, middlewares.Logger(middlewares.Auth(route.Function))).Methods(route.Method)
		} else {
			r.HandleFunc(route.URI, middlewares.Logger(route.Function)).Methods(route.Method)
		}

	}

	assetsPath := "/assets/"

	fileServer := http.FileServer(http.Dir("." + filepath.FromSlash(assetsPath)))
	r.PathPrefix(assetsPath).Handler(http.StripPrefix(assetsPath, fileServer))

	return r
}
