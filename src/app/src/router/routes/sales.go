package routes

import (
	"eurest-utils-app/src/controllers"
	"net/http"
)

var salesRoutes = []Route{
	{
		URI:         "/missingSales",
		Method:      http.MethodGet,
		Function:    controllers.LoadMissingSales,
		RequireAuth: true,
	},
	{
		URI:         "/missingDocsAT",
		Method:      http.MethodGet,
		Function:    controllers.LoadMissingDocsAT,
		RequireAuth: true,
	},
	{
		URI:         "/missingSales/{year}",
		Method:      http.MethodGet,
		Function:    controllers.GetMissingSales,
		RequireAuth: true,
	},
	{
		URI:         "/missingDocsAT/{store}",
		Method:      http.MethodGet,
		Function:    controllers.GetMissingDocsAT,
		RequireAuth: true,
	},
}
