package models

type Lojas struct {
	Codigo    uint64 `json:"codigo"`
	Descricao string `json:"descricao"`
}
