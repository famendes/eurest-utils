package models

type MissingSales struct {
	Loja         int    `json:"loja,omitempty"`
	Unidade      int    `json:"unidade,omitempty"`
	IDLocal      int    `json:"id_local,omitempty"`
	Documento    string `json:"documento,omitempty"`
	Serie        string `json:"serie,omitempty"`
	UltDocEurest int    `json:"ultimo_doc_eurest,omitempty"`
	UltCom       string `json:"ultima_comunicacao,omitempty"`
	UltDocDc     int    `json:"ultimo_doc_dc,omitempty"`
	DataUltDocDc string `json:"data_ultimo_doc_dc,omitempty"`
	Falta        int    `json:"falta,omitempty"`
	LastUpdate   string `json:"last_update,omitempty"`
	Estado       string `json:"estado,omitempty"`
	NomeUnidade  string `json:"nome_unidade,omitempty"`
}

type MissingDocsAT struct {
	Loja        int    `json:"loja,omitempty"`
	Unidade     int    `json:"unidade,omitempty"`
	IDLocal     int    `json:"id_local,omitempty"`
	Documento   string `json:"documento,omitempty"`
	Serie       string `json:"serie,omitempty"`
	NaoEnviados int    `json:"nao_enviados,omitempty"`
}
