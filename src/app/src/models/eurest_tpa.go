package models

type EurestTPA struct {
	Loja     uint64 `json:"loja,omitempty"`
	Caixa    uint64 `json:"caixa,omitempty"`
	Processo string `json:"processo,omitempty"`
	IDTPA    string `json:"id_tpa,omitempty"`
}
