package models

type AuthData struct {
	Id       string `json:"id"`
	Username string `json:"username"`
	Name     string `json:"nome"`
	Token    string `json:"token"`
}
