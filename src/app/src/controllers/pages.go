package controllers

import (
	"eurest-utils-app/src/cookies"
	"eurest-utils-app/src/utils"
	"net/http"
)

func LoadLogin(w http.ResponseWriter, r *http.Request) {
	cookie, _ := cookies.Read(r)

	if cookie["token"] != "" {
		http.Redirect(w, r, "/home", 302)
		return
	}
	utils.ExecuteTemplate(w, "login.html", nil)
}

func LoadHome(w http.ResponseWriter, r *http.Request) {
	cookie, _ := cookies.Read(r)
	name, _ := cookie["name"]
	userName, _ := cookie["username"]
	utils.ExecuteTemplate(w, "home.html", struct {
		Name     string
		Username string
	}{Name: name, Username: userName})
}

func LoadTPA(w http.ResponseWriter, r *http.Request) {
	utils.ExecuteTemplate(w, "tpa.html", nil)
}

func LoadMissingSales(w http.ResponseWriter, r *http.Request) {
	utils.ExecuteTemplate(w, "missing_sales.html", nil)
}

func LoadMissingDocsAT(w http.ResponseWriter, r *http.Request) {
	utils.ExecuteTemplate(w, "missing_docs_at.html", nil)
}
