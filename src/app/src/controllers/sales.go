package controllers

import (
	"encoding/json"
	"eurest-utils-app/src/config"
	"eurest-utils-app/src/models"
	"eurest-utils-app/src/requests"
	"eurest-utils-app/src/responses"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func GetMissingSales(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	year := params["year"]

	url := fmt.Sprintf("%s/missingSales/%s", config.ApiUrl, year)
	response, err := requests.AuthRequest(r, http.MethodGet, url, nil)
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ApiError{Err: err.Error()})
		return
	}
	defer response.Body.Close()

	if response.StatusCode >= 400 {
		responses.HandleStatusCodeError(w, response)
		return
	}

	var missingSales []models.MissingSales
	if err := json.NewDecoder(response.Body).Decode(&missingSales); err != nil {
		responses.JSON(w, http.StatusUnprocessableEntity, responses.ApiError{Err: err.Error()})
		return
	}

	responses.JSON(w, http.StatusOK, missingSales)
}

func GetMissingDocsAT(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	store := params["store"]

	url := fmt.Sprintf("%s/missingDocsAT/%s", config.ApiUrl, store)
	response, err := requests.AuthRequest(r, http.MethodGet, url, nil)
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ApiError{Err: err.Error()})
		return
	}
	defer response.Body.Close()

	if response.StatusCode >= 400 {
		responses.HandleStatusCodeError(w, response)
		return
	}

	var missingDocsAT []models.MissingDocsAT
	if err := json.NewDecoder(response.Body).Decode(&missingDocsAT); err != nil {
		responses.JSON(w, http.StatusUnprocessableEntity, responses.ApiError{Err: err.Error()})
		return
	}

	responses.JSON(w, http.StatusOK, missingDocsAT)
}
