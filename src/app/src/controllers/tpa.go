package controllers

import (
	"bytes"
	"encoding/json"
	"eurest-utils-app/src/config"
	"eurest-utils-app/src/models"
	"eurest-utils-app/src/requests"
	"eurest-utils-app/src/responses"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddTPA(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	loja, _ := strconv.ParseUint(r.FormValue("loja"), 10, 64)
	caixa, _ := strconv.ParseUint(r.FormValue("caixa"), 10, 64)
	processo := r.FormValue("processo")
	idTpa := r.FormValue("id_tpa")

	tpaContract, err := json.Marshal(map[string]interface{}{
		"loja":     loja,
		"caixa":    caixa,
		"processo": processo,
		"id_tpa":   idTpa,
	})
	fmt.Printf("loja:%d, caixa:%d, processo: %s, id_tpa: %s", loja, caixa, processo, idTpa)
	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ApiError{Err: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/tpa", config.ApiUrl)
	response, err := requests.AuthRequest(r, http.MethodPost, url, bytes.NewBuffer(tpaContract))
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ApiError{Err: err.Error()})
		return
	}
	defer response.Body.Close()

	if response.StatusCode >= 400 {
		responses.HandleStatusCodeError(w, response)
		return
	}

	responses.JSON(w, response.StatusCode, nil)
}

func GetTPAsByStore(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	storeID, err := strconv.ParseUint(params["storeID"], 10, 64)
	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ApiError{Err: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/tpa/%d", config.ApiUrl, storeID)
	response, err := requests.AuthRequest(r, http.MethodGet, url, nil)
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ApiError{Err: err.Error()})
		return
	}
	defer response.Body.Close()

	if response.StatusCode >= 400 {
		responses.HandleStatusCodeError(w, response)
		return
	}

	var eurestTPAs []models.EurestTPA
	if err := json.NewDecoder(response.Body).Decode(&eurestTPAs); err != nil {
		responses.JSON(w, http.StatusUnprocessableEntity, responses.ApiError{Err: err.Error()})
		return
	}

	responses.JSON(w, response.StatusCode, eurestTPAs)
}
