package controllers

import (
	"encoding/json"
	"eurest-utils-app/src/config"
	"eurest-utils-app/src/models"
	"eurest-utils-app/src/requests"
	"eurest-utils-app/src/responses"
	"fmt"
	"net/http"
)

func GetStores(w http.ResponseWriter, r *http.Request) {
	url := fmt.Sprintf("%s/stores", config.ApiUrl)
	response, err := requests.AuthRequest(r, http.MethodGet, url, nil)
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ApiError{Err: err.Error()})
		return
	}

	defer response.Body.Close()

	if response.StatusCode >= 400 {
		responses.HandleStatusCodeError(w, response)
		return
	}

	var lojas []models.Lojas
	if err := json.NewDecoder(response.Body).Decode(&lojas); err != nil {
		responses.JSON(w, http.StatusUnprocessableEntity, responses.ApiError{Err: err.Error()})
		return
	}

	responses.JSON(w, response.StatusCode, lojas)
}
