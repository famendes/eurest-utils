package controllers

import (
	"bytes"
	"encoding/json"
	"eurest-utils-app/src/config"
	"eurest-utils-app/src/cookies"
	"eurest-utils-app/src/models"
	"eurest-utils-app/src/responses"
	"fmt"
	"net/http"
)

func Login(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	user, err := json.Marshal(map[string]string{
		"username": r.FormValue("username"),
		"password": r.FormValue("password"),
	})

	if err != nil {
		responses.JSON(w, http.StatusBadRequest, responses.ApiError{Err: err.Error()})
		return
	}

	url := fmt.Sprintf("%s/login", config.ApiUrl)
	response, err := http.Post(url, "application/json", bytes.NewBuffer(user))
	if err != nil {
		responses.JSON(w, http.StatusInternalServerError, responses.ApiError{Err: err.Error()})
		return
	}

	defer response.Body.Close()

	if response.StatusCode >= 400 {
		responses.HandleStatusCodeError(w, response)
		return
	}

	var authData models.AuthData
	if err := json.NewDecoder(response.Body).Decode(&authData); err != nil {
		responses.JSON(w, http.StatusUnprocessableEntity, responses.ApiError{Err: err.Error()})
		return
	}

	if err := cookies.Save(w, authData.Id, authData.Username, authData.Name, authData.Token); err != nil {
		responses.JSON(w, http.StatusUnprocessableEntity, responses.ApiError{Err: err.Error()})
		return
	}
	responses.JSON(w, response.StatusCode, authData)
}
