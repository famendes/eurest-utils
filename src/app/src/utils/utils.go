package utils

import (
	"html/template"
	"net/http"
	"path/filepath"
)

var templates *template.Template

func LoadTemplates() {
	viewsPath := "views/*.html"
	templatesPath := "views/templates/*.html"

	templates = template.Must(template.ParseGlob(filepath.FromSlash(viewsPath)))
	templates = template.Must(templates.ParseGlob(filepath.FromSlash(templatesPath)))
}

func ExecuteTemplate(w http.ResponseWriter, template string, data interface{}) {
	templates.ExecuteTemplate(w, template, data)
}
