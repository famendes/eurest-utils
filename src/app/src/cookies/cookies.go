package cookies

import (
	"eurest-utils-app/src/config"
	"net/http"
	"time"

	"github.com/gorilla/securecookie"
)

var s *securecookie.SecureCookie

func Config() {
	s = securecookie.New(config.HashKey, config.BlockKey)
}

func Save(w http.ResponseWriter, id, username, name, token string) error {
	data := map[string]string{
		"id":       id,
		"username": username,
		"name":     name,
		"token":    token,
	}

	encodedData, err := s.Encode("DATA", data)
	if err != nil {
		return err
	}

	http.SetCookie(w, &http.Cookie{
		Name:     "DATA",
		Value:    encodedData,
		Path:     "/",
		HttpOnly: true,
	})

	return nil
}

func Read(r *http.Request) (map[string]string, error) {
	cookie, err := r.Cookie("DATA")
	if err != nil {
		return nil, err
	}

	values := make(map[string]string)
	if err := s.Decode("DATA", cookie.Value, &values); err != nil {
		return nil, err
	}
	return values, nil
}

func Delete(w http.ResponseWriter) {
	http.SetCookie(w, &http.Cookie{
		Name:     "DATA",
		Value:    "",
		Path:     "/",
		HttpOnly: true,
		Expires:  time.Unix(0, 0),
	})
}
