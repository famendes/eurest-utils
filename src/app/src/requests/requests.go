package requests

import (
	"eurest-utils-app/src/cookies"
	"io"
	"net/http"
)

func AuthRequest(r *http.Request, method, url string, data io.Reader) (*http.Response, error) {
	request, err := http.NewRequest(method, url, data)
	if err != nil {
		return nil, err
	}

	cookie, _ := cookies.Read(r)
	request.Header.Add("Authorization", "Bearer "+cookie["token"])
	cliente := &http.Client{}
	response, err := cliente.Do(request)
	if err != nil {
		return nil, err
	}

	return response, nil
}
