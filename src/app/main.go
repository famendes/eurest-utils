package main

import (
	"eurest-utils-app/src/config"
	"eurest-utils-app/src/cookies"
	"eurest-utils-app/src/router"
	"eurest-utils-app/src/utils"
	"fmt"
	"log"
	"net/http"
)

func init() {

	// Gerar HashKey e BlockKey
	// hashKey := hex.EncodeToString(securecookie.GenerateRandomKey(16))
	// fmt.Println(hashKey)
	// blockKey := hex.EncodeToString(securecookie.GenerateRandomKey(16))
	// fmt.Println(blockKey)
}

func main() {

	config.Load()
	cookies.Config()
	utils.LoadTemplates()
	r := router.Generate()
	fmt.Printf("Webapp listenning on port: %d\n", config.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.Port), r))
}
