var tpaCollection = [];

function GetTpaContractsByStore() {

    $('#tpaContracts tbody').html('');
    let storeID = $('#stores').val()

    $.ajax({
        url: `/tpa/${storeID}`,
        method: 'GET',
        contentType: "application/json;charset=utf-8"
    }).done(function(response) {
        var tableLines = '';
        $.each(response, function(index, data) {
            tableLines += `<tr id="contract${index+1}" scope="row">
                            <th class="align-middle">${data.loja}</th>
                            <td class="align-middle">${data.caixa}</td>
                            <td class="align-middle">${data.processo}</td>
                            <td class="align-middle">${data.id_tpa}</td>
                            </tr>`
        });

        $('#tpaContracts tbody').append(tableLines);
        $("#tpaContracts:hidden").show( "fast" );
        tpaCollection = response;
    }).fail(function(error) {
        console.log(error);
    });
}

function openAddTPAModal() {

    $('#caixa').val('');
    $('#processo').val('');
    $('#idTpa').val('');
    let storeID = $('#stores').val();
    if (storeID == "0") {
        alert("Selecione uma loja.");
    } else {
        $('#store').val(storeID)
        $('#modalAddTPA').modal('show');
    }
}

function saveTPA() {

    let storeID  = $('#store').val()
    let caixa    = $('#caixa').val()
    let processo = $('#processo').val()
    let idTpa    = $('#idTpa').val()
    let data     = {
        loja     : storeID,
        caixa    : caixa,
        processo : processo,
        id_tpa   : idTpa,
    }
    $.each(tpaCollection, function(index, data) {
        if (data.id_tpa == idTpa) {
            alert('O ID TPA já existe para esta loja.')
            return false
        }
    })

    $.ajax({
        url         : '/tpa',
        method      : 'POST',
        data        : data
    }).done(function() {
        console.log("success")
    }).fail(function(error) {
        console.log(error);
    });
}