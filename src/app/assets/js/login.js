$('#login').on('submit', login);

function login (e) {
    e.preventDefault();

    let username = $('#username').val();
    let password = $('#password').val();

    $.ajax({
        url: '/login',
        method: 'POST',
        data: {
            username: username,
            password: password,
        },
    }).done(function() {
        window.location = "/home"
    }).fail(function(error) {
        console.log(error)
        alert("Erro no login")
    });
}