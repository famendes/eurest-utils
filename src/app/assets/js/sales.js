function GetMissingSales() {

    $('#missingSales tbody').html('');
    let year = $('#year').val()

    if (year == "") {
        alert("O ano não pode ser vazio.");
    }

    $.ajax({
        url: `/missingSales/${year}`,
        method: 'GET',
        contentType: "application/json;charset=utf-8"
    }).done(function(response) {
        var tableLines = '';
        $.each(response, function(index, data) {
            tableLines += `<tr id="msale${index+1}" scope="row">
                            <th class="align-middle">${data.loja}</th>
                            <td class="align-middle">${data.unidade}</td>
                            <td class="align-middle">${data.id_local}</td>
                            <td class="align-middle">${data.documento}</td>
                            <td class="align-middle">${data.serie}</td>
                            <td class="align-middle">${data.ultimo_doc_eurest}</td>
                            <td class="align-middle">${data.ultima_comunicacao}</td>
                            <td class="align-middle">${data.ultimo_doc_dc}</td>
                            <td class="align-middle">${data.data_ultimo_doc_dc}</td>
                            <td class="align-middle">${data.falta}</td>
                            <td class="align-middle">${data.last_update}</td>
                            <td class="align-middle">${data.estado}</td>
                            <td class="align-middle">${data.nome_unidade}</td>
                            </tr>`
        });

        $('#missingSales tbody').append(tableLines);
        $("#missingSales:hidden").show( "fast" );
    }).fail(function(error) {
        console.log(error);
    });
}

function GetMissingDocsAT() {
    $('#missingDocsAT tbody').html('');
    let store = $('#stores').val()

    $.ajax({
        url: `/missingDocsAT/${store}`,
        method: 'GET',
        contentType: "application/json;charset=utf-8"
    }).done(function(response) {
        var tableLines = '';
        $.each(response, function(index, data) {
            tableLines += `<tr id="msdocAT${index+1}" scope="row">
                            <th class="align-middle">${data.loja}</th>
                            <td class="align-middle">${data.unidade}</td>
                            <td class="align-middle">${data.id_local}</td>
                            <td class="align-middle">${data.documento}</td>
                            <td class="align-middle">${data.serie}</td>
                            <td class="align-middle">${data.nao_enviados}</td>
                            </tr>`
        });

        $('#missingDocsAT tbody').append(tableLines);
        $("#missingDocsAT:hidden").show( "fast" );
    }).fail(function(error) {
        console.log(error);
    });
}