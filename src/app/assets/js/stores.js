$(document).ready(function()
{
    LoadStores();
});

function LoadStores() {
    $.ajax({
        url: '/stores',
        method: 'GET',
        contentType: "application/json;charset=utf-8"
    }).done(function(response) {
        $('#stores').append($.map(response, function(r) {
            return $('<option/>', {
                value: r.codigo,
                text: r.codigo + " - " + r.descricao
            });
        }));
    }).fail(function(error) {
        console.log(error);
    });
}