import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import mitt from 'mitt'
import setupInterceptors from './services/setupInterceptors';
setupInterceptors(store);

const app = createApp(App)

const emitter = mitt()
app.config.globalProperties.emitter = emitter
app.use(store)
app.use(router)
app.config.globalProperties.$filters = {
    formatDateTime(dt) {
        if(!dt) return ''

        let d = dt.split('T')
    
        let date = d[0]
        let time = d[1]
    
        time = time.split('+')
        time = time[0]
    
        return date + ' ' + time
    }
  }
app.mount('#app')
