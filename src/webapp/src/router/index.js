import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '@/views/LoginView.vue'
import TokenService from "@/services/token.service"

const HomeView = () => import(/* webpackChunkName: "home" */'@/views/HomeView.vue')
const TpaComponent = () => import(/* webpackChunkName: "tpa" */'@/components/tpa/Tpa.vue')
const MissingSalesComponent = () => import(/* webpackChunkName: "tpa" */'@/components/sales/MissingSales.vue')
const ATLastDocComponent = () => import(/* webpackChunkName: "tpa" */'@/components/docs_at/DocsAt.vue')

const routes = [
	{
		path: '/',
		name: 'login',
		component: LoginView
	},
	{
		path: '/home',
		name: 'home',
		component: HomeView,
		children: [
			{
				path: '/tpa',
				name: 'tpa',
				component: TpaComponent,
			},
			{
				path: '/sales',
				name: 'sales',
				component: MissingSalesComponent
			},
			{
				path: '/atlastdoc',
				name: 'atlastdoc',
				component: ATLastDocComponent
			}
			
		]
	}
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to) => {
	let token = TokenService.getLocalAccessToken()
	if (!token && to.name !== 'login') {
		return { name: 'login' }
	}
	return true
  })

export default router
