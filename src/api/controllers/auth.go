package controllers

import (
	"encoding/json"
	"eurest-utils-api/auth"
	"eurest-utils-api/config"
	"eurest-utils-api/database"
	"eurest-utils-api/enums"
	"eurest-utils-api/models"
	"eurest-utils-api/repositories"
	"eurest-utils-api/responses"
	"eurest-utils-api/security"
	"io/ioutil"
	"net/http"
	"strconv"
)

func Login(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.Err(
			w,
			http.StatusUnprocessableEntity,
			models.ErrorMsg{
				Err:     enums.UnprocessableEntityCode,
				Message: enums.UnprocessableEntityMsg,
				Data:    err,
			},
		)
		return
	}

	var user models.User
	if err := json.Unmarshal(body, &user); err != nil {
		responses.Err(
			w,
			http.StatusBadRequest,
			models.ErrorMsg{
				Err:     enums.BadRequestCode,
				Message: enums.BadRequestMsg,
				Data:    err,
			},
		)
		return
	}

	db, err := database.ConnectAuth()
	if err != nil {
		responses.Err(
			w,
			http.StatusInternalServerError,
			models.ErrorMsg{
				Err:     enums.InternalServerErrorCode,
				Message: enums.InternalServerErrorMsg,
				Data:    err,
			},
		)
		return
	}
	defer db.Close()

	serial := config.Serial
	rep := repositories.NewUserRepository(db)
	savedUser, err := rep.GetUserBySerial(user.UserName, serial)
	if err != nil {
		responses.Err(
			w,
			http.StatusInternalServerError,
			models.ErrorMsg{
				Err:     enums.InternalServerErrorCode,
				Message: enums.InternalServerErrorMsg,
				Data:    err,
			},
		)
		return
	}

	if savedUser.ID == 0 {
		responses.Err(
			w,
			http.StatusUnauthorized,
			models.ErrorMsg{
				Err:     enums.UnauthorizedCode,
				Message: enums.UnauthorizedMsg,
				Data:    err,
			},
		)
		return
	}

	if err := security.CheckPassword(savedUser.Password, user.Password); err != nil {
		responses.Err(
			w,
			http.StatusUnauthorized,
			models.ErrorMsg{
				Err:     enums.UnauthorizedCode,
				Message: enums.UnauthorizedMsg,
				Data:    err,
			},
		)
		return
	}

	token, err := auth.CreateToken(savedUser.ID)
	if err != nil {
		responses.Err(
			w,
			http.StatusInternalServerError,
			models.ErrorMsg{
				Err:     enums.InternalServerErrorCode,
				Message: enums.InternalServerErrorMsg,
				Data:    err,
			},
		)
		return
	}

	userID := strconv.FormatUint(savedUser.ID, 10)
	name := savedUser.Nome
	userName := savedUser.UserName
	responses.JSON(w, http.StatusOK, models.AuthData{Id: userID, Username: userName, Nome: name, Token: token})
}
