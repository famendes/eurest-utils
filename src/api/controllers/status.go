package controllers

import (
	"eurest-utils-api/responses"
	"net/http"
)

func Status(w http.ResponseWriter, r *http.Request) {
	msg := "API up and running!!"
	responses.JSON(w, http.StatusOK, msg)
}
