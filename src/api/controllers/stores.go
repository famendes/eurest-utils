package controllers

import (
	"eurest-utils-api/database"
	"eurest-utils-api/enums"
	"eurest-utils-api/models"
	"eurest-utils-api/repositories"
	"eurest-utils-api/responses"
	"net/http"
)

func GetStores(w http.ResponseWriter, r *http.Request) {
	db, err := database.ConnectApp()
	if err != nil {
		responses.Err(
			w,
			http.StatusInternalServerError,
			models.ErrorMsg{
				Err:     enums.InternalServerErrorCode,
				Message: enums.InternalServerErrorMsg,
				Data:    err,
			},
		)
		return
	}
	defer db.Close()

	rep := repositories.NewStoresRepository(db)
	stores, err := rep.GetAllStores()
	if err != nil {
		responses.Err(
			w,
			http.StatusInternalServerError,
			models.ErrorMsg{
				Err:     enums.InternalServerErrorCode,
				Message: enums.InternalServerErrorMsg,
				Data:    err,
			},
		)
		return
	}

	responses.JSON(w, http.StatusOK, stores)
}
