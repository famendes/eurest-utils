package controllers

import (
	"encoding/json"
	"eurest-utils-api/database"
	"eurest-utils-api/enums"
	"eurest-utils-api/models"
	"eurest-utils-api/repositories"
	"eurest-utils-api/responses"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddTPA(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.Err(
			w,
			http.StatusUnprocessableEntity,
			models.ErrorMsg{
				Err:     enums.UnprocessableEntityCode,
				Message: enums.UnprocessableEntityMsg,
				Data:    err,
			},
		)
		return
	}

	var eurestTpa models.EurestTPA
	if err := json.Unmarshal(body, &eurestTpa); err != nil {
		responses.Err(
			w,
			http.StatusBadRequest,
			models.ErrorMsg{
				Err:     enums.BadRequestCode,
				Message: enums.BadRequestMsg,
				Data:    err,
			},
		)
		return
	}

	db, err := database.ConnectApp()
	if err != nil {
		responses.Err(
			w,
			http.StatusInternalServerError,
			models.ErrorMsg{
				Err:     enums.DBConnectErrorCode,
				Message: enums.DBConnectErrorMsg,
				Data:    err,
			},
		)
		return
	}
	defer db.Close()

	rep := repositories.NewTpaRepository(db)
	rowsAffected, err := rep.AddTPA(eurestTpa)
	if err != nil {
		responses.Err(
			w,
			http.StatusInternalServerError,
			models.ErrorMsg{
				Err:     enums.InternalServerErrorCode,
				Message: enums.InternalServerErrorMsg,
				Data:    err,
			},
		)
		return
	}

	responses.JSON(w, http.StatusCreated, rowsAffected)
}

func GetTPAsByStore(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	storeID, err := strconv.ParseUint(params["storeID"], 10, 64)
	if err != nil {
		responses.Err(
			w,
			http.StatusUnprocessableEntity,
			models.ErrorMsg{
				Err:     enums.UnprocessableEntityCode,
				Message: enums.UnprocessableEntityMsg,
				Data:    err,
			},
		)
		return
	}

	db, err := database.ConnectApp()
	if err != nil {
		responses.Err(
			w,
			http.StatusInternalServerError,
			models.ErrorMsg{
				Err:     enums.DBConnectErrorCode,
				Message: enums.DBConnectErrorMsg,
				Data:    err,
			},
		)
		return
	}
	defer db.Close()

	rep := repositories.NewTpaRepository(db)
	eurestTPAs, err := rep.GetTPAsByStore(storeID)
	if err != nil {
		responses.Err(
			w,
			http.StatusInternalServerError,
			models.ErrorMsg{
				Err:     enums.DBConnectErrorCode,
				Message: enums.DBConnectErrorMsg,
				Data:    err,
			},
		)
		return
	}

	responses.JSON(w, http.StatusOK, eurestTPAs)
}
