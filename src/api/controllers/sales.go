package controllers

import (
	"eurest-utils-api/cache"
	"eurest-utils-api/config"
	"eurest-utils-api/enums"
	"eurest-utils-api/models"
	"eurest-utils-api/responses"
	"eurest-utils-api/services"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func MissingSales(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	year := params["year"]

	ch := cache.NewRedisCache(config.RedisHost, config.RedisDB, time.Duration(config.RedisExp))
	key := fmt.Sprintf("ms_%s", year)
	data := ch.Get(key)
	if data != nil {
		responses.JSON(w, http.StatusOK, data)
		return
	}

	service := services.NewSalesService()
	missingSales, err := service.GetAllMissingSalesByYear(year)
	if err != nil {
		responses.Err(
			w,
			http.StatusInternalServerError,
			models.ErrorMsg{
				Err:     enums.InternalServerErrorCode,
				Message: enums.InternalServerErrorMsg,
				Data:    err,
			},
		)
		return
	}

	ch.Set(key, missingSales)

	responses.JSON(w, http.StatusOK, missingSales)
}

func MissingDocsAT(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	store := params["store"]

	service := services.NewSalesService()
	missingDocsAT, err := service.GetAllMissingDocsAT(store)
	if err != nil {
		responses.Err(
			w,
			http.StatusInternalServerError,
			models.ErrorMsg{
				Err:     enums.InternalServerErrorCode,
				Message: enums.InternalServerErrorMsg,
				Data:    err,
			},
		)
		return
	}

	responses.JSON(w, http.StatusOK, missingDocsAT)
}

func ATLastSentDoc(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	unit := params["unit"]

	service := services.NewSalesService()
	atLastSentDoc, err := service.GetATLastSentDocByUnit(unit)
	if err != nil {
		responses.Err(
			w,
			http.StatusInternalServerError,
			models.ErrorMsg{
				Err:     enums.InternalServerErrorCode,
				Message: enums.InternalServerErrorMsg,
				Data:    err,
			},
		)
		return
	}
	responses.JSON(w, http.StatusOK, atLastSentDoc)
}
