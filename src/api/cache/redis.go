package cache

import (
	"encoding/json"
	"log"
	"time"

	"github.com/go-redis/redis"
)

type redisCache struct {
	host    string
	db      int
	expires time.Duration
}

func NewRedisCache(host string, db int, exp time.Duration) Cache {
	return &redisCache{
		host:    host,
		db:      db,
		expires: exp,
	}
}

func (cache *redisCache) getClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     cache.host,
		Password: "",
		DB:       cache.db,
	})
}

func (cache *redisCache) Set(key string, value interface{}) {
	json, err := json.Marshal(value)
	if err != nil {
		log.Fatal(err)
	}

	client := cache.getClient()
	client.Set(key, json, cache.expires*time.Second)
}
func (cache *redisCache) Get(key string) interface{} {
	client := cache.getClient()

	item, err := client.Get(key).Result()
	if err != nil {
		return nil
	}
	return item
}
