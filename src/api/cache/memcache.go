package cache

import (
	"encoding/json"
	"github.com/bradfitz/gomemcache/memcache"
	"log"
)

type memCache struct {
	host []string
}

func NewMemCache(host []string) Cache {
	return &memCache{
		host: host,
	}
}

func (cache *memCache) getclient() *memcache.Client {
	mc := memcache.New(cache.host...)
	return mc
}

func (cache *memCache) Set(key string, value interface{}) {
	bytes, err := json.Marshal(value)
	if err != nil {
		log.Fatal(err)
	}
	client := cache.getclient()
	if err := client.Set(&memcache.Item{Key: key, Value: bytes}); err != nil {
		log.Fatal(err)
	}
}

func (cache *memCache) Get(key string) interface{} {
	client := cache.getclient()
	item, err := client.Get(key)
	if err != nil {
		log.Fatal(err)
	}
	return item
}
