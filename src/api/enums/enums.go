package enums

const (
	DBConnectErrorCode = "database_connection_error"
	DBConnectErrorMsg  = "There was a problem connecting to the database."

	PasswordMismatchCode = "pwd_mismatch"
	PasswordMismatchMsg  = "Wrong password."

	UserMismatchCode = "userid_mismatch"
	UserMismatchMsg  = "User is not the same that is logged in."

	UnprocessableEntityCode = "unprocessable_entity"
	UnprocessableEntityMsg  = "There was a problem processing your request."

	BadRequestCode = "bad_request"
	BadRequestMsg  = "Your request structure is invalid or incorrect."

	InternalServerErrorCode = "internal_server_error"
	InternalServerErrorMsg  = "Server error."

	UnauthorizedCode = "unauthorized"
	UnauthorizedMsg  = "You don't have permission to access this resource."
)
