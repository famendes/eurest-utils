package services

import (
	"eurest-utils-api/models"
	"eurest-utils-api/repositories"
	"strconv"
)

type SalesService interface {
	GetAllMissingSalesByYear(year string) ([]models.MissingSales, error)
	GetAllMissingDocsAT(store string) ([]models.MissingDocsAT, error)
	GetATLastSentDocByUnit(unit string) ([]models.ATLastSentDoc, error)
}

type salesService struct{}

func NewSalesService() SalesService {
	return &salesService{}
}

func (*salesService) GetAllMissingSalesByYear(year string) ([]models.MissingSales, error) {
	var shortYear string
	if len(year) > 2 {
		shortYear = string(year[len(year)-2:])
	}

	salesRepo := repositories.NewSalesRepository()
	missingSales, err := salesRepo.GetAllMissingSalesByYear(shortYear)
	if err != nil {
		return nil, err
	}
	return missingSales, nil
}

func (*salesService) GetAllMissingDocsAT(store string) ([]models.MissingDocsAT, error) {
	storeConv, err := strconv.Atoi(store)
	if err != nil {
		return nil, err
	}
	salesRepo := repositories.NewSalesRepository()
	missingDocsAT, err := salesRepo.GetAllMissingDocsAT(storeConv)
	if err != nil {
		return nil, err
	}
	return missingDocsAT, nil
}

func (*salesService) GetATLastSentDocByUnit(unit string) ([]models.ATLastSentDoc, error) {
	unitConv, err := strconv.Atoi(unit)
	if err != nil {
		return nil, err
	}

	salesRepo := repositories.NewSalesRepository()
	atLastSentDoc, err := salesRepo.GetATLastSentDocByUnit(unitConv)
	if err != nil {
		return nil, err
	}
	return atLastSentDoc, nil
}
