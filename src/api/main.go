package main

import (
	"eurest-utils-api/config"
	"eurest-utils-api/router"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
)

// func init() {
// 	// Function to generate secret key
// 	key := make([]byte, 64)
// 	if _, err := rand.Read(key); err != nil {
// 		log.Fatal(err)
// 	}
// 	stringBase64 := base64.StdEncoding.EncodeToString(key)
// 	fmt.Println(stringBase64)
// }

func main() {
	config.Load()
	fmt.Printf("Eurest Utils API Listening on port: %d\n", config.Port)
	r := router.Generate()
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.Port), handlers.CORS(headersOk, methodsOk, originsOk)(r)))
}
