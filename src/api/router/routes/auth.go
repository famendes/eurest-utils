package routes

import (
	"eurest-utils-api/controllers"
	"net/http"
)

var authRoutes = []Route{
	{
		URI:         "/api/login",
		Method:      http.MethodPost,
		Function:    controllers.Login,
		RequireAuth: false,
	},
	// {
	// 	URI: "api/refresh",
	// 	Method: http.MethodPost,
	// 	Function: controllers.Refresh,
	// 	RequireAuth: false,
	// },
}
