package routes

import (
	"eurest-utils-api/controllers"
	"net/http"
)

var storesRoutes = []Route{
	{
		URI:         "/api/stores",
		Method:      http.MethodGet,
		Function:    controllers.GetStores,
		RequireAuth: true,
	},
}
