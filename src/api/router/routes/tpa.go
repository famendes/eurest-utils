package routes

import (
	"eurest-utils-api/controllers"
	"net/http"
)

var tpaRoutes = []Route{
	{
		URI:         "/api/tpa",
		Method:      http.MethodPost,
		Function:    controllers.AddTPA,
		RequireAuth: true,
	},
	{
		URI:         "/api/tpa/{storeID}",
		Method:      http.MethodGet,
		Function:    controllers.GetTPAsByStore,
		RequireAuth: true,
	},
}
