package routes

import (
	"eurest-utils-api/controllers"
	"net/http"
)

var salesRoutes = []Route{
	{
		URI:         "/api/missingSales/{year}",
		Method:      http.MethodGet,
		Function:    controllers.MissingSales,
		RequireAuth: true,
	},
	{
		URI:         "/api/missingDocsAT/{store}",
		Method:      http.MethodGet,
		Function:    controllers.MissingDocsAT,
		RequireAuth: true,
	},
	{
		URI:         "/api/atLastSentDoc/{unit}",
		Method:      http.MethodGet,
		Function:    controllers.ATLastSentDoc,
		RequireAuth: true,
	},
}
