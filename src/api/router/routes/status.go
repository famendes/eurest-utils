package routes

import (
	"eurest-utils-api/controllers"
	"net/http"
)

var statusRoute = Route{
	URI:         "/api/status",
	Method:      http.MethodGet,
	Function:    controllers.Status,
	RequireAuth: false,
}
