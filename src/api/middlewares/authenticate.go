package middlewares

import (
	"eurest-utils-api/auth"
	"eurest-utils-api/enums"
	"eurest-utils-api/models"
	"eurest-utils-api/responses"
	"net/http"
)

func Authenticate(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := auth.ValidateToken(r); err != nil {
			responses.Err(
				w,
				http.StatusUnauthorized,
				models.ErrorMsg{
					Err:     enums.UnauthorizedCode,
					Message: enums.UnauthorizedMsg,
					Data:    err,
				},
			)
			return
		}
		next(w, r)
	}
}
