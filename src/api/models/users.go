package models

type User struct {
	ID       uint64 `json:"id,omitempty"`
	Nif      string `json:"nif,omitempty"`
	UserName string `json:"username,omitempty"`
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
	Serial   string `json:"serial,omitempty"`
	Nome     string `json:"nome,omitempty"`
}
