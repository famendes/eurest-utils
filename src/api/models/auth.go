package models

type AuthData struct {
	Id       string `json:"id"`
	Username string `json:"username"`
	Nome     string `json:"nome"`
	Token    string `json:"token"`
}
