package models

type ErrorMsg struct {
	Err     string      `json:"error"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}
