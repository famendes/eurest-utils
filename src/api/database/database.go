package database

import (
	"database/sql"
	"errors"
	"eurest-utils-api/config"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func connect(server string) (*sql.DB, error) {

	var connectionString string
	var dialect string

	switch server {
	case "app":
		connectionString = config.AppConnectionString
		dialect = config.AppDialect
	case "auth":
		connectionString = config.AuthConnectionString
		dialect = config.AuthDialect
	default:
		return nil, errors.New("Invalid type of connection")
	}

	db, err := sql.Open(dialect, connectionString)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		db.Close()
		return nil, err
	}

	return db, nil
}

// ConnectApp opens a connection to the application database
// and returns a pointer to that connection.
func ConnectApp() (*sql.DB, error) {
	client, err := connect("app")
	if err != nil {
		log.Fatal(err)
	}

	return client, nil
}

// ConnectAuth opens a connection to the authorization database
// and returns a pointer to that connection.
func ConnectAuth() (*sql.DB, error) {
	client, err := connect("auth")
	if err != nil {
		log.Fatal(err)
	}

	return client, nil
}
