package config

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

var (
	AppDialect             = ""
	AuthDialect            = ""
	AuthConnectionString   = ""
	AppConnectionString    = ""
	BrokerProtocol         = ""
	BrokerConnectionString = ""
	Port                   = 0
	Env                    = ""
	Serial                 = ""
	BurstLimit             = 3
	SecretKey              []byte
	RedisHost              = ""
	RedisPort              = 0
	RedisDB                = 0
	RedisExp               = 60
)
var err error

// Load is used to configure all the app settings
func Load() {

	if godotenv.Load(); err != nil {
		log.Fatal(err)
	}
	configureCommon()
	configureApp()
	configureAuth()
	configureRedis()
	configureBroker()
}

func configureCommon() {
	Port, err = strconv.Atoi(os.Getenv("API_PORT"))
	if err != nil {
		log.Fatal(err)
	}

	SecretKey = []byte(os.Getenv("SECRET_KEY"))

	BurstLimit, err = strconv.Atoi(os.Getenv("BURST_LIMIT"))
	if err != nil {
		log.Fatal(err)
	}

	Env = os.Getenv("ENV")

	Serial = os.Getenv(fmt.Sprintf("%s_SERIAL", Env))

}

func configureApp() {
	AppDialect = os.Getenv("CLIENT_DB_DIALECT")

	AppConnectionString = fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=True&loc=Local",
		os.Getenv(fmt.Sprintf("%s_CLIENT_DB_USER", Env)),
		os.Getenv(fmt.Sprintf("%s_CLIENT_DB_PASSWORD", Env)),
		os.Getenv(fmt.Sprintf("%s_CLIENT_DB_HOST", Env)),
		os.Getenv(fmt.Sprintf("%s_CLIENT_DB_PORT", Env)),
		os.Getenv(fmt.Sprintf("%s_CLIENT_DB_NAME", Env)),
		os.Getenv(fmt.Sprintf("%s_CLIENT_DB_CHARSET", Env)),
	)
}

func configureAuth() {
	AuthDialect = os.Getenv("AUTH_DB_DIALECT")

	AuthConnectionString = fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=True&loc=Local",
		os.Getenv(fmt.Sprintf("%s_AUTH_DB_USER", Env)),
		os.Getenv(fmt.Sprintf("%s_AUTH_DB_PASSWORD", Env)),
		os.Getenv(fmt.Sprintf("%s_AUTH_DB_HOST", Env)),
		os.Getenv(fmt.Sprintf("%s_AUTH_DB_PORT", Env)),
		os.Getenv(fmt.Sprintf("AUTH_DB_NAME")),
		os.Getenv(fmt.Sprintf("%s_AUTH_DB_CHARSET", Env)),
	)
}

func configureRedis() {
	RedisHost = os.Getenv(os.Getenv("REDIS_HOST"))
	RedisPort, err = strconv.Atoi(os.Getenv("REDIS_PORT"))
	if err != nil {
		log.Fatal(err)
	}

	RedisExp, err = strconv.Atoi(os.Getenv("REDIS_EXP"))
	if err != nil {
		log.Fatal(err)
	}

	RedisDB, err = strconv.Atoi(os.Getenv("REDIS_DB"))
	if err != nil {
		log.Fatal(err)
	}
}

func configureBroker() {
	BrokerProtocol = os.Getenv("RABBIT_BROKER_PROTOCOL")
	BrokerConnectionString = fmt.Sprintf(
		"%s:%s@%s:%s",
		os.Getenv("RABBIT_BROKER_USERNAME"),
		os.Getenv("RABBIT_BROKER_PASSWORD"),
		os.Getenv("RABBIT_BROKER_HOST"),
		os.Getenv("RABBIT_BROKER_PORT"),
	)
}
