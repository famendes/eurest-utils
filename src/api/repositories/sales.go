package repositories

import (
	"eurest-utils-api/database"
	"eurest-utils-api/models"
	"fmt"
)

type SalesRepository interface {
	GetAllMissingSalesByYear(year string) ([]models.MissingSales, error)
	GetAllMissingDocsAT(store int) ([]models.MissingDocsAT, error)
	GetATLastSentDocByUnit(unit int) ([]models.ATLastSentDoc, error)
}

type salesRepository struct {
}

func NewSalesRepository() SalesRepository {
	return &salesRepository{}
}

func (*salesRepository) GetAllMissingSalesByYear(year string) ([]models.MissingSales, error) {
	client, err := database.ConnectApp()
	if err != nil {
		return nil, err
	}
	defer client.Close()

	results, err := client.Query(
		`SELECT 
			e.loja AS 'Loja',
			u.unidade AS 'Unidade',
			u.id_local AS 'ID Local',
			e.doc AS 'Documento',
			e.serie 'Série',
			e.numero AS 'Últ. Num. Eurest',
			e.exportado AS 'Últ. Comunicação',
			max(d.numero) AS 'Últ. Num. DC',
			max(d.datahora) AS 'Data/Hora Últ. Doc. DC',
			max(d.numero)-e.numero AS 'Em falta',
			lj.lastupdate AS 'LastUpdate',
			IF ((now()-lj.lastupdate) > 30000,'Offline','Online') AS 'Estado',
			u.descricao AS 'Nome Unidade'
		FROM eurest_export e
		LEFT JOIN eurest_unidades u ON u.loja=e.loja
		LEFT JOIN documentos d ON d.loja=e.loja AND d.doc=e.doc AND d.serie=e.serie
		LEFT JOIN lojas lj ON lj.codigo=e.loja
		WHERE e.serie LIKE ?
		AND e.anulado = 0
		AND e.doc IN ('FS','FT','FA','CC','NC')
		GROUP BY e.loja, u.unidade, u.id_local, e.doc, e.serie, e.numero, u.descricao, lj.lastupdate
		HAVING (max(d.numero)-e.numero) > 0
		ORDER BY max(d.numero)-e.numero DESC`,
		"%"+year+"%",
	)

	if err != nil {
		return nil, err
	}
	defer results.Close()

	var missingSales []models.MissingSales
	for results.Next() {
		var missingSalesLine models.MissingSales
		if err := results.Scan(
			&missingSalesLine.Loja,
			&missingSalesLine.Unidade,
			&missingSalesLine.IDLocal,
			&missingSalesLine.Documento,
			&missingSalesLine.Serie,
			&missingSalesLine.UltDocEurest,
			&missingSalesLine.UltCom,
			&missingSalesLine.UltDocDc,
			&missingSalesLine.DataUltDocDc,
			&missingSalesLine.Falta,
			&missingSalesLine.LastUpdate,
			&missingSalesLine.Estado,
			&missingSalesLine.NomeUnidade,
		); err != nil {
			return nil, err
		}
		missingSales = append(missingSales, missingSalesLine)
	}
	return missingSales, nil
}

func (*salesRepository) GetAllMissingDocsAT(store int) ([]models.MissingDocsAT, error) {
	client, err := database.ConnectApp()
	if err != nil {
		return nil, err
	}
	defer client.Close()

	condition := ""
	if store != 0 {
		condition = fmt.Sprintf("AND d.loja = %d", store)
	}

	query := `SELECT d.loja, eu.unidade, eu.id_local, d.doc, d.serie, COUNT(d.numero) AS NaoEnviados
			FROM documentos d
			INNER JOIN eurest_unidades eu ON d.loja = eu.loja
			WHERE d.sync_at = 0
			AND d.doc IN ('FA','FS','FT','NC')
			` + condition + `
			GROUP BY 1, 2, 3, 4, 5
			ORDER BY 1, 2, 3, 4, 5`

	results, err := client.Query(query)
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var missingDocsAT []models.MissingDocsAT
	for results.Next() {
		var missingDocsATLine models.MissingDocsAT
		if err := results.Scan(
			&missingDocsATLine.Loja,
			&missingDocsATLine.Unidade,
			&missingDocsATLine.IDLocal,
			&missingDocsATLine.Documento,
			&missingDocsATLine.Serie,
			&missingDocsATLine.NaoEnviados,
		); err != nil {
			return nil, err
		}
		missingDocsAT = append(missingDocsAT, missingDocsATLine)
	}
	return missingDocsAT, nil
}

func (*salesRepository) GetATLastSentDocByUnit(unit int) ([]models.ATLastSentDoc, error) {
	client, err := database.ConnectApp()
	if err != nil {
		return nil, err
	}
	defer client.Close()

	condition := ""
	if unit != 0 {
		condition = fmt.Sprintf("AND eu.unidade = %d", unit)
	}

	query := `SELECT 
				d.loja,
				eu.unidade,
				eu.id_local,
				eu.descricao,
				d.doc,
				d.serie,
				d.numero,
				min(d.datahora)
			FROM documentos d
			INNER JOIN eurest_unidades eu ON d.loja = eu.loja
			WHERE d.sync_at = 0
			AND d.doc in ('FS', 'FA', 'FR', 'NC')
			AND eu.activa = 1
			AND eu.AT = 1
			AND eu.unidade != 0
			` + condition + `
			GROUP BY d.loja`

	results, err := client.Query(query)
	if err != nil {
		return nil, err
	}

	defer results.Close()

	var atLastSentDoc []models.ATLastSentDoc
	for results.Next() {
		var record models.ATLastSentDoc
		if err := results.Scan(
			&record.LojaZS,
			&record.UnidadeEurest,
			&record.IDLocal,
			&record.NomeUnidade,
			&record.Documento,
			&record.Serie,
			&record.Numero,
			&record.DataHora,
		); err != nil {
			return nil, err
		}
		atLastSentDoc = append(atLastSentDoc, record)
	}
	return atLastSentDoc, nil
}
