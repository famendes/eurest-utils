package repositories

import (
	"database/sql"
)

type auth struct {
	db *sql.DB
}

func NewAuthRepository(db *sql.DB) *auth {
	return &auth{db}
}
