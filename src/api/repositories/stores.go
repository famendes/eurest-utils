package repositories

import (
	"database/sql"
	"eurest-utils-api/models"
)

type lojas struct {
	db *sql.DB
}

func NewStoresRepository(db *sql.DB) *lojas {
	return &lojas{db}
}

func (lojasRep lojas) GetAllStores() ([]models.Lojas, error) {
	results, err := lojasRep.db.Query(
		`SELECT l.codigo, l.descricao 
		FROM lojas l
		INNER JOIN eurest_unidades eu ON l.codigo = eu.loja
		WHERE l.codigo NOT IN (0)
		AND eu.activa = 1`,
	)
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var stores []models.Lojas
	for results.Next() {
		var store models.Lojas
		if err := results.Scan(
			&store.Codigo,
			&store.Descricao,
		); err != nil {
			return nil, err
		}
		stores = append(stores, store)
	}

	return stores, nil
}
