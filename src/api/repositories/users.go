package repositories

import (
	"database/sql"
	"eurest-utils-api/models"
)

type users struct {
	db *sql.DB
}

func NewUserRepository(db *sql.DB) *users {
	return &users{db}
}

func (userRep users) GetUserBySerial(userName, serial string) (models.User, error) {
	result, err := userRep.db.Query(
		`SELECT id, nif, username, email, passwd, serial, nome
			FROM tblUserAcesso
			WHERE serial = ? AND username = ?`,
		serial,
		userName,
	)
	if err != nil {
		return models.User{}, err
	}
	defer result.Close()

	var user models.User
	if result.Next() {
		if err := result.Scan(
			&user.ID,
			&user.Nif,
			&user.UserName,
			&user.Email,
			&user.Password,
			&user.Serial,
			&user.Nome,
		); err != nil {
			return models.User{}, nil
		}
	}

	return user, nil
}
