package repositories

import (
	"database/sql"
	"eurest-utils-api/models"
)

type tpa struct {
	db *sql.DB
}

func NewTpaRepository(db *sql.DB) *tpa {
	return &tpa{db}
}

func (tpaRep tpa) AddTPA(contract models.EurestTPA) (uint64, error) {
	stmt, err := tpaRep.db.Prepare("INSERT INTO eurest_tpas (loja, caixa, processo, idtpa) VALUES (?,?,?,?)")
	if err != nil {
		return 0, err
	}
	defer stmt.Close()

	result, err := stmt.Exec(contract.Loja, contract.Caixa, contract.Processo, contract.IDTPA)
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return uint64(rowsAffected), nil
}

func (tpaRep tpa) GetTPAsByStore(storeID uint64) ([]models.EurestTPA, error) {
	results, err := tpaRep.db.Query(
		"SELECT loja, caixa, processo, idtpa FROM eurest_tpas WHERE loja = ?",
		storeID,
	)
	if err != nil {
		return nil, err
	}
	defer results.Close()

	var eurestTPAs []models.EurestTPA
	for results.Next() {
		var eurestTPA models.EurestTPA
		if err := results.Scan(
			&eurestTPA.Loja,
			&eurestTPA.Caixa,
			&eurestTPA.Processo,
			&eurestTPA.IDTPA,
		); err != nil {
			return nil, err
		}
		eurestTPAs = append(eurestTPAs, eurestTPA)
	}

	return eurestTPAs, nil
}
