package security

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
)

func CheckPassword(pwdHash, pwdString string) error {
	hashedPwd := getMD5Hash(pwdString)
	if pwdHash != hashedPwd {
		errors.New("invalid_password")
	}
	return nil
}

func getMD5Hash(pwd string) string {
	hash := md5.Sum([]byte(pwd))
	return hex.EncodeToString(hash[:])
}
